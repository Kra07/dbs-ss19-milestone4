<?php echo form_open('store/add'); ?>

<h1>Add a Store</h1>
<?php echo validation_errors(); ?>
<div id="body">

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="NAME">Name</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="NAME" />
    </div>
  </div>

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="RATING">Rating</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="RATING" />
    </div>
  </div>

  <input type="submit" name="submit" value="Submit" />

</div>
