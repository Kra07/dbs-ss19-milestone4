<h1>Stores</h1>
<div id="body">

<div>
<table>
  <tr>
    <th>Name</th>
    <th>Rating</th>
    <th style="text-align:right" >Edit</th>
    <th style="text-align:right" >Delete</th>
  </tr>
  <?php foreach ($store as $item): ?>
  <tr>
    <td class="elem"><?php echo $item['NAME']; ?></td>
    <td class="elem"><?php echo $item['RATING']; ?></td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('store/update/'.$item['ID']); ?>">O</a>
    </td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('store/del/'.$item['ID']); ?>">X</a>
    </td>
  </tr>
  <?php endforeach; ?>
</table> 
</div>

<p>
  <a class="bbutton" href="<?php echo site_url('store/add'); ?>">Add Store</a>
</p>

</div>
