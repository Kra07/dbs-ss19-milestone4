<?php echo form_open('product/add'); ?>

<h1>Add a Product</h1>
<?php echo validation_errors(); ?>
<div id="body">

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="NAME">Name</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="NAME" />
    </div>
  </div>

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="PRICE">Price</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="number" name="PRICE" />
    </div>
  </div>

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="DESCRIPTION">Description</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="DESCRIPTION" />
    </div>
  </div>

  <input type="submit" name="submit" value="Submit" />

</div>
