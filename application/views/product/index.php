<h1>Products</h1>
<div id="body">

<div>
<table>
  <tr>
    <th>Name</th>
    <th>PRICE</th>
    <th>DESCRIPTION</th>
    <th style="text-align:right" >Delete</th>
  </tr>
  <?php foreach ($product as $item): ?>
  <tr>
    <td class="elem"><?php echo $item['NAME']; ?></td>
    <td class="elem"><?php echo $item['PRICE']; ?></td>
    <td class="elem"><?php echo $item['DESCRIPTION']; ?></td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('product/del/'.$item['ID']); ?>">X</a>
    </td>
  </tr>
  <?php endforeach; ?>
</table> 
</div>

<p>
  <a class="bbutton" href="<?php echo site_url('product/add'); ?>">Add Product</a>
</p>

</div>
