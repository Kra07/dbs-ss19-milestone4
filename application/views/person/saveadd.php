<?php echo form_open( 'person/add_save/'.$ID ); ?>

<h1>Add Savings</h1>
<?php echo validation_errors(); ?>
<div id="body">

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="AMOUNT">Amount</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="number" name="AMOUNT">
    </div>
  </div>

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="CDATE">Date</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="date" name="CDATE">
    </div>
  </div>

  <input type="submit" name="submit" value="Submit"/>

</div>
