<?php echo form_open('person/add'); ?>

<h1>Add a Person</h1>
<?php echo validation_errors(); ?>
<div id="body">

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="NAME">Name</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="NAME" />
    </div>
  </div>

  <div class="row responsive-label">
    <div class="col-sm-12 col-md-3">
      <label for="SHORTNAME">Short Name</label>
    </div>
    <div class="col-sm-12 col-md">
      <input type="input" name="SHORTNAME" />
    </div>
  </div>

  <input type="submit" name="submit" value="Submit" />

</div>
