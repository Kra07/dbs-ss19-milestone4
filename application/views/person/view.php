<h1>Accounting</h1>
<div id="body">

<h2><?php echo $person['NAME'] ; ?></h2>

<div id="bordered">
<h1>Savings</h1>
<table>
  <tr>
    <th>Amount</th>
    <th>Date</th>
    <th style="text-align:right" >Delete</th>
  </tr>
  <?php foreach ($account as $item): ?>
  <tr>
    <td class="elem"><?php echo $item['AMOUNT']; ?> €</td>
    <td class="elem"><?php echo $item['CDATE']; ?></td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('person'); ?>">X</a>
    </td>
  </tr>
  <?php endforeach; ?>
</table> 
<p>
  <a class="sbutton" href="<?php echo site_url('person/add_save/'.$person['ID'] ); ?>">Add</a>
</p>
</div>

<div id="bordered">
<h1>Expenses</h1>
<table>
    <th>Amount</th>
    <th>Date</th>
    <th style="text-align:right" >Delete</th>
  <?php foreach ($expenses as $item): ?>
  <tr>
    <td class="elem"><?php echo $item['AMOUNT']; ?> €</td>
    <td class="elem"><?php echo $item['CDATE']; ?></td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('person'); ?>">X</a>
    </td>
  </tr>
  <?php endforeach; ?>
</table> 
<p>
  <a class="sbutton" href="<?php echo site_url('person/add_exp/'.$person['ID'] ); ?>">Add</a>
</p>
</div>

<p>
  <a class="bbutton" href="<?php echo site_url('person'); ?>">Back</a>
  <a class="bbutton" href="<?php echo site_url('person/bill/'.$person['ID']); ?>">Add Bill</a>
</p>

</div>
