<h1>Person</h1>
<div id="body">

<div>
<table>
  <tr>
    <th>Name</th>
    <th>SHORTNAME</th>
    <th style="text-align:right" >Delete</th>
  </tr>
  <?php foreach ($person as $item): ?>
  <tr>
    <td class="elem"><a href="<?php echo site_url('person/view/'.$item['ID']); ?>"><?php echo $item['NAME']; ?></a></td>
    <td class="elem"><?php echo $item['SHORTNAME']; ?></td>
    <td style="text-align:right" >
      <a class="sbutton" href="<?php echo site_url('person/del/'.$item['ID']); ?>">X</a>
    </td>
  </tr>
  <?php endforeach; ?>
</table> 
</div>

<p>
  <a class="bbutton" href="<?php echo site_url('person/add'); ?>">Add Person</a>
</p>

</div>
