<?php
class Product extends CI_Controller {
  protected $database;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url_helper');
    $this->load->helper('form');
    $this->load->helper('date'); //nice_date
    $this->load->library('form_validation');
    require_once APPPATH.'/models/Oracledb.php';
    $this->database = new Oracledb();
  }

  private function _layout( $table = NULL, $data = NULL )
  {
    $data['path']    = 'product';

    $this->load->view('templates/header');
    $this->load->view( $table, $data);
    $this->load->view('templates/footer');
  }

  public function index()
  {
    $data['product'] = $this->database->get_product();
    $this->_layout('product/index', $data);
  }

  public function add()
  {
    $this->form_validation->set_rules('NAME', 'Name', 'required');
    $this->form_validation->set_rules('PRICE', 'PRICE', 'required');
    $this->form_validation->set_rules('DESCRIPTION', 'DESCRIPTION', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->_layout( 'product/add' );
    }
    else
    {
      $data['message'] = $this->database->set_product(
        $this->input->post('NAME'),
        $this->input->post('PRICE'),
        $this->input->post('DESCRIPTION')
      );
      $this->_layout( 'success', $data );
    }
  }

  public function del( $ID = NULL )
  {
	$data['message'] = $this->database->del_product( $ID );
    $this->_layout( 'success', $data );
  }

  public function add_mult()
  {
  }
}
