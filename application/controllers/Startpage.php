<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Startpage extends CI_Controller {
    protected $database;

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        require_once APPPATH.'/models/Oracledb.php';
        $this->database = new Oracledb();
    }

	public function index()
	{
    $this->load->view('templates/header');
	$this->load->view('welcome');
    $this->load->view('templates/footer');
	}
}
