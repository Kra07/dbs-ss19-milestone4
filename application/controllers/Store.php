<?php
class Store extends CI_Controller {
  protected $database;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url_helper');
    $this->load->helper('form');
    $this->load->helper('date'); //nice_date
    $this->load->library('form_validation');
    require_once APPPATH.'/models/Oracledb.php';
    $this->database = new Oracledb();
  }

  private function _layout( $table = NULL, $data = NULL )
  {
    $data['path']    = 'store';
  
    $this->load->view('templates/header');
    $this->load->view( $table, $data);
    $this->load->view('templates/footer');
  }

  public function index()
  {
    $data['store'] = $this->database->get_store();
    $this->_layout('store/index', $data);
  }

  public function add()
  {
    $this->form_validation->set_rules('NAME', 'Name', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->_layout( 'store/add' );
    }
    else
    {
      $data['message'] = $this->database->set_store(
        $this->input->post('NAME'),
        $this->input->post('RATING')
      );
      $this->_layout( 'success', $data );
    }
  }

  public function del( $ID = NULL )
  {
    $data['message'] = $this->database->del_store( $ID );
    $this->_layout( 'success', $data );
  }

  public function update( $ID = NULL )
  {
    $this->form_validation->set_rules('NAME', 'Name', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $data['item'] = $this->database->get_store( $ID );
      $this->_layout( 'store/update', $data );
    }
    else
    {
      $data['message'] = $this->database->update_store(
        $ID,
        $this->input->post('NAME'),
        $this->input->post('RATING')
      );
      $this->_layout( 'success', $data );
    }
  }
}
