<?php
class Person extends CI_Controller {
  protected $database;

  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url_helper');
    $this->load->helper('form');
    $this->load->helper('date'); //nice_date
    $this->load->library('form_validation');
    require_once APPPATH.'/models/Oracledb.php';
    $this->database = new Oracledb();
  }

  private function _layout( $table = NULL, $data = NULL )
  {
    $data['path']    = 'person';

    $this->load->view('templates/header');
    $this->load->view( $table, $data);
    $this->load->view('templates/footer');
  }

  public function index()
  {
    $data['person'] = $this->database->get_person();
    $this->_layout('person/index', $data);
  }

  public function add()
  {
    $this->form_validation->set_rules('NAME', 'Name', 'required');
    $this->form_validation->set_rules('SHORTNAME', 'SHORTNAME', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $this->_layout( 'person/add' );
    }
    else
    {
      $data['message'] = $this->database->set_person(
        $this->input->post('NAME'),
        $this->input->post('SHORTNAME')
      );
      $this->_layout( 'success', $data );
    }
  }

  public function del( $ID = NULL )
  {
    $data['message'] = $this->database->del_person( $ID );
    $this->_layout( 'success', $data );
  }

  public function view( $ID = NULL )
  {
    $data['person'] = $this->database->get_person( $ID );
    $data['account'] = $this->database->get_account( $ID );
    $data['expenses'] = $this->database->get_expenses( $ID);

    if (empty($data['person']))
    {
      show_404();
    }

    $this->_layout( 'person/view', $data );

  }

  public function add_exp( $PERSON_ID = NULL )
  {
    $this->form_validation->set_rules('AMOUNT', 'AMOUNT', 'required');
    $this->form_validation->set_rules('CDATE', 'CDATE', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $data['ID'] = $PERSON_ID;
      $this->_layout( 'person/expadd', $data );
    }
    else
    {
      $data['message'] = $this->database->set_expadd(
        $this->input->post('AMOUNT'),
        nice_date( $this->input->post('CDATE'), 'd-M-y'),
        $PERSON_ID
      );
      $this->_layout( 'success', $data );
    }
  }

  public function del_exp( $PERSON_ID = NULL )
  {
    $data['message'] = $this->database->del_expadd( $PERSON_ID );
    $this->_layout( 'success', $data );
  }

  public function add_save( $PERSON_ID = NULL )
  {
    $this->form_validation->set_rules('AMOUNT', 'AMOUNT', 'required');
    $this->form_validation->set_rules('CDATE', 'CDATE', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $data['ID'] = $PERSON_ID;
      $this->_layout( 'person/saveadd', $data );
    }
    else
    {
      $data['message'] = $this->database->set_saveadd(
        $this->input->post('AMOUNT'),
        nice_date( $this->input->post('CDATE'), 'd-M-y'),
        $PERSON_ID
      );
      $this->_layout( 'success', $data );
    }
  }

  public function del_save( $PERSON_ID = NULL )
  {
    $data['message'] = $this->database->del_saveadd( $PERSON_ID );
    $this->_layout( 'success', $data );
  }

  public function bill( $PERSON_ID = NULL )
  {
    $data['pid']   = $PERSON_ID;
    $data['store'] = $this->database->get_store();
    $this->_layout( 'person/bill', $data);
  }

  public function entrys()
  {
    $PERSON_ID = $this->input->get('person');
    $STORE_ID  = $this->input->get('store');

    $this->form_validation->set_rules('AMOUNT', 'AMOUNT', 'required');
    $this->form_validation->set_rules('CDATE', 'CDATE', 'required');

    if ($this->form_validation->run() === FALSE)
    {
      $data['store'] = $this->database->get_products( $STORE_ID );
      $this->_layout( 'person/entrys', $data );
    }
    else
    {
      /*
      $data['message'] = $this->database->set_entrys(
        $this->input->post('AMOUNT'),
        nice_date( $this->input->post('CDATE'), 'd-M-y'),
        $this->input->post('product'),
        $PERSON_ID
      );
       */
      $data['message'] = 'Operation not implemented';
      $this->_layout( 'success', $data );
    }
  }
}
