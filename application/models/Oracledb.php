<?php
class Oracledb {

  // Since the connection details are constant, define them as const
  const username = 'a01227531';
  const password = 'dbs19';
  const con_string = '(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = oracle-lab.cs.univie.ac.at)(PORT = 1521)))(CONNECT_DATA=(SID = lab)))';

  // $conn is set in the constructor.
  protected $conn;

  /**
   * Create connection with the command oci_connect(
   * String(username), String(password), String(connection_string))
   * The @ sign avoids the output of warnings
   */
  public function __construct()
  {
    putenv("NLS_LANG=AMERICAN_AMERICA.AL32UTF8");
    try
    {
      $this->conn = @oci_connect(
        Oracledb::username,
        Oracledb::password,
        Oracledb::con_string
      );

      //check if the connection object is != null
      if( ! $this->conn )
      {
        die("DB error: Connection can't be established!");
      }
    }
    catch (Exception $e)
    {
      die("DB error: {$e->getMessage()}");
    }
  }

  public function __destruct()
  {
    // clean up
    @oci_close($this->conn);
  }

  public function _db_exec( $statement )
  {
    $objExecute = @oci_execute($statement);
    $ret = "Operation failed. ";

    if($objExecute)
    {
      @oci_commit($this->conn);
      $ret = "Operation successful.";
    }
    else
    {
      @oci_rollback($this->conn);
      $error = oci_error($statement);
      $ret .= '<br>'.$error['message'];
    }

    return $ret;
  }

  public function get_person($ID = FALSE)
  {
    if ($ID === FALSE)
    {
      $sql = "SELECT * FROM person";
      $statement = @oci_parse($this->conn, $sql);
      @oci_execute($statement);
      @oci_fetch_all($statement, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
      @oci_free_statement($statement);
      return $res;
    }
    else
    {
      $sql = "SELECT * FROM person WHERE ID LIKE :person_id";
      $statement = @oci_parse($this->conn, $sql);
      @oci_bind_by_name($statement, ':person_id', $ID);
      @oci_execute($statement);
      $res = @oci_fetch_assoc($statement);
      @oci_free_statement($statement);
      return $res;
    }
  }

  public function set_person( $NAME, $SHORTNAME )
  {
    $sql = "INSERT INTO PERSON (NAME, SHORTNAME) VALUES ( :NAME, :SHORTNAME )";
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':NAME', $NAME);
    @oci_bind_by_name($statement, ':SHORTNAME', $SHORTNAME);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function del_person($ID)
  {
    $errorcode = 0;
    $sql = 'BEGIN PERSON_DEL(:ID, :errorcode); END;';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':ID', $ID);
    @oci_bind_by_name($statement, ':errorcode', $errorcode);
    @oci_execute($statement);
    @oci_free_statement($statement);
    return $errorcode;
  }

  public function get_store($ID = FALSE)
  {
    if ($ID === FALSE)
    {
      $sql = "SELECT * FROM seller";
      $statement = @oci_parse($this->conn, $sql);
      @oci_execute($statement);
      @oci_fetch_all($statement, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
      @oci_free_statement($statement);
      return $res;
    }
    else
    {
      $sql = 'SELECT * FROM seller WHERE ID LIKE :store_id';
      $statement = @oci_parse($this->conn, $sql);
      @oci_bind_by_name($statement, ':store_id', $ID);
      @oci_execute($statement);
      $res = @oci_fetch_assoc($statement);
      @oci_free_statement($statement);
      return $res;
    }
  }

  public function set_store( $NAME, $RATING )
  {
    $sql = "INSERT INTO seller (NAME, RATING) VALUES ( :NAME, :RATING )";
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':NAME', $NAME);
    @oci_bind_by_name($statement, ':RATING', $RATING);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function update_store( $ID, $NAME, $RATING )
  {
    $sql = "UPDATE SELLER SET NAME=:NAME, RATING=:RATING WHERE ID=:ID";
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':ID', $ID);
    @oci_bind_by_name($statement, ':NAME', $NAME);
    @oci_bind_by_name($statement, ':RATING', $RATING);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function del_store($ID)
  {
    $sql = 'delete from SELLER where SELLER.ID = :seller_id';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':seller_id', $ID);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function get_product($ID = FALSE)
  {
    if ($ID === FALSE)
    {
      $sql = "SELECT * FROM product";
      $statement = @oci_parse($this->conn, $sql);
      @oci_execute($statement);
      @oci_fetch_all($statement, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
      @oci_free_statement($statement);
      return $res;
    }
    else
    {
      $sql = 'SELECT * FROM product WHERE ID LIKE :product_id';
      $statement = @oci_parse($this->conn, $sql);
      @oci_bind_by_name($statement, ':product_id', $ID);
      @oci_execute($statement);
      $res = @oci_fetch_assoc($statement);
      @oci_free_statement($statement);
      return $res;
    }
  }

  public function set_product( $NAME, $PRICE, $DESCRIPTION )
  {
    $sql = 'INSERT INTO product (NAME, PRICE, DESCRIPTION) VALUES (:NAME, :PRICE, :DESCRIPTION)';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':NAME', $NAME);
    @oci_bind_by_name($statement, ':PRICE', $PRICE);
    @oci_bind_by_name($statement, ':DESCRIPTION', $DESCRIPTION);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function del_product($ID)
  {
    $sql = 'delete from product where product.ID = :product_id';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':product_id', $ID);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function get_account($ID)
  {
    $sql = "SELECT * FROM ACCOUNT WHERE PERSON_ID = :person_id";
    return $this->get_table( $ID, $sql);
  }

  public function get_expenses($ID)
  {
    $sql = "SELECT * FROM EXPENSES WHERE PERSON_ID = :person_id";
    return $this->get_table( $ID, $sql);
  }

  public function get_table($ID, $sql)
  {
      $statement = @oci_parse($this->conn, $sql);
      @oci_bind_by_name($statement, ':person_id', $ID);
      @oci_execute($statement);
      @oci_fetch_all($statement, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
      @oci_free_statement($statement);
      return $res;
  }

  public function set_expadd( $AMOUNT, $CDATE, $PERSON_ID )
  {
    $sql = 'INSERT INTO EXPENSES (AMOUNT, CDATE, PERSON_ID) VALUES (:AMOUNT, :CDATE, :PERSON_ID)';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':AMOUNT', $AMOUNT);
    @oci_bind_by_name($statement, ':CDATE', $CDATE);
    @oci_bind_by_name($statement, ':PERSON_ID', $PERSON_ID);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function set_saveadd( $AMOUNT, $CDATE, $PERSON_ID )
  {
    $sql = 'INSERT INTO ACCOUNT (AMOUNT, CDATE, PERSON_ID) VALUES (:AMOUNT, :CDATE, :PERSON_ID)';
    $statement = @oci_parse($this->conn, $sql);
    @oci_bind_by_name($statement, ':AMOUNT', $AMOUNT);
    @oci_bind_by_name($statement, ':CDATE', $CDATE);
    @oci_bind_by_name($statement, ':PERSON_ID', $PERSON_ID);
    $success = $this->_db_exec($statement);
    @oci_free_statement($statement);
    return $success;
  }

  public function get_products( $SELLER_ID )
  {
      $sql = 'SELECT * FROM products WHERE SELLER_ID LIKE :SELLER_ID';
      $statement = @oci_parse($this->conn, $sql);
      @oci_bind_by_name($statement, ':SELLER_ID', $SELLER_ID);
      @oci_execute($statement);
      @oci_fetch_all($statement, $res, null, null, OCI_FETCHSTATEMENT_BY_ROW);
      @oci_free_statement($statement);
      return $res;
  }

  public function set_entrys( $AMOUNT, $CDATE, $product, $PERSON_ID )
  {
  }
}
