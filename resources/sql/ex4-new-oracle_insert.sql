insert into PERSON ( ID, SHORTNAME, NAME ) values ( 1, 'kk', 'Kolja Kauer' );

insert into PRODUCT ( ID, NAME, PRICE, DESCRIPTION ) values ( 1, 'Schoko Kekse', 2, 'Billig und süß' );
insert into PRODUCT ( ID, NAME, PRICE, DESCRIPTION ) values ( 2, 'Wasch Set', 20, 'Farb und Weiß Waschen zusammen' );
insert into PRODUCT ( ID, NAME, PRICE, DESCRIPTION ) values ( 3, 'Farbwäsche', 13, 'farbige Wäsche');
insert into PRODUCT ( ID, NAME, PRICE, DESCRIPTION ) values ( 4, 'Weißewäsche', 12, 'Weißerriese' );

insert into SELLER ( ID, NAME, RATING ) values ( 1, 'Billa', 'gut und günstig' );

insert into ACCOUNT ( ID, AMOUNT, CDATE, PERSON_ID ) values ( 1, 100, '16-JUN-19', 1 );

insert into EXPENSES ( ID, AMOUNT, cdate, PERSON_ID ) values ( 1, 6, '16-JUN-19', 1 );

insert into BILL ( ID, cdate, EXPENSES_ID ) values ( 1, '16-JUN-19', 1 );

insert into CONSISTS ( ID, PRODUCT1_ID, PRODUCT2_ID ) values ( 1, 2, 3 );
insert into CONSISTS ( ID, PRODUCT1_ID, PRODUCT2_ID ) values ( 2, 2, 4 );

insert into ENTRY ( BILL_ID, PRODUCT_ID, QUANTITY ) values ( 1, 1, 3 );

insert into SELLS ( ID, PRODUCT_ID, SELLER_ID ) values ( 1, 1, 1 );
insert into SELLS ( ID, PRODUCT_ID, SELLER_ID ) values ( 2, 2, 1 );
