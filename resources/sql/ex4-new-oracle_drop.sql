-- DROP

DROP VIEW Products;
DROP VIEW Savings;

ALTER TABLE Account
    DROP CONSTRAINT Account_Person;
ALTER TABLE Bill
    DROP CONSTRAINT Bill_Expenses;
ALTER TABLE Expenses
    DROP CONSTRAINT Expenses_Person;
ALTER TABLE consists
    DROP CONSTRAINT consists_Product1;
ALTER TABLE consists
    DROP CONSTRAINT consists_Product2;
ALTER TABLE Entry
    DROP CONSTRAINT Entry_Bill;
ALTER TABLE Entry
    DROP CONSTRAINT Entry_Product;
ALTER TABLE sells
    DROP CONSTRAINT sells_Product;
ALTER TABLE sells
    DROP CONSTRAINT sells_Seller;

DROP SEQUENCE Account_seq;
DROP SEQUENCE Bill_seq;
DROP SEQUENCE Expenses_seq;
DROP SEQUENCE Person_seq;
DROP SEQUENCE Product_seq;
DROP SEQUENCE Seller_seq;
DROP SEQUENCE consists_seq;
DROP SEQUENCE sells_seq;

DROP TRIGGER Account_bir ;
DROP TRIGGER Bill_bir ;
DROP TRIGGER Expenses_bir ;
DROP TRIGGER Person_bir ;
DROP TRIGGER Product_bir ;
DROP TRIGGER Seller_bir ;
DROP TRIGGER consists_bir ;
DROP TRIGGER sells_bir ;

DROP PROCEDURE Person_del ;

DROP TABLE Account;
DROP TABLE Bill;
DROP TABLE Expenses;
DROP TABLE Person;
DROP TABLE Product;
DROP TABLE Seller;
DROP TABLE consists;
DROP TABLE Entry;
DROP TABLE sells;

-- End of file.
