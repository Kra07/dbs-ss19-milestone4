-- Create

-- Table: Person
CREATE TABLE Person (
    ID NUMBER(10) NOT NULL,
    SHORTNAME varchar2(64)  NULL,
    NAME varchar2(64)  NOT NULL,
    CONSTRAINT Person_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Person_seq START WITH 1;

CREATE OR REPLACE TRIGGER Person_bir 
BEFORE INSERT ON Person 
FOR EACH ROW
BEGIN
  SELECT Person_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

CREATE OR REPLACE PROCEDURE Person_del(
   p_id  IN  Person.id%TYPE,
   p_error_code OUT NUMBER
)
AS
  BEGIN
    DELETE
    FROM person
    WHERE p_id = Person.id;
 
    p_error_code := SQL%ROWCOUNT;
    IF (p_error_code = 1)
    THEN
      COMMIT;
    ELSE
      ROLLBACK;
    END IF;
    EXCEPTION
    WHEN OTHERS
    THEN
      p_error_code := SQLCODE;
  END Person_del;
/

-- Table: Product
CREATE TABLE Product (
    ID NUMBER(10)  NOT NULL,
    NAME varchar2(64)  NOT NULL,
    PRICE NUMBER(10)  NOT NULL,
    DESCRIPTION varchar2(64)  NOT NULL,
    CONSTRAINT Product_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Product_seq START WITH 1;

CREATE OR REPLACE TRIGGER Product_bir 
BEFORE INSERT ON Product 
FOR EACH ROW
BEGIN
  SELECT Product_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Table: Seller
CREATE TABLE Seller (
    ID NUMBER(10)  NOT NULL,
    NAME varchar2(64)  NOT NULL,
    RATING varchar2(64)  NULL,
    CONSTRAINT Seller_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Seller_seq START WITH 1;

CREATE OR REPLACE TRIGGER Seller_bir 
BEFORE INSERT ON Seller 
FOR EACH ROW
BEGIN
  SELECT Seller_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Table: Account
CREATE TABLE Account (
    ID NUMBER(10)  NOT NULL,
    AMOUNT NUMBER(10)  NOT NULL,
    CDATE date  NOT NULL,
    PERSON_ID NUMBER(10)  NOT NULL,
    CONSTRAINT Account_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Account_seq START WITH 1;

CREATE OR REPLACE TRIGGER Account_bir 
BEFORE INSERT ON Account 
FOR EACH ROW
BEGIN
  SELECT Account_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Reference: Account_Person (table: Account)
ALTER TABLE Account ADD CONSTRAINT Account_Person
    FOREIGN KEY (Person_ID)
    REFERENCES Person (ID)
    on delete cascade;

-- Table: Expenses
CREATE TABLE Expenses (
    ID NUMBER(10)  NOT NULL,
    AMOUNT NUMBER(10)  NOT NULL,
    CDATE date  NOT NULL,
    PERSON_ID NUMBER(10)  NOT NULL,
    CONSTRAINT Expenses_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Expenses_seq START WITH 1;

CREATE OR REPLACE TRIGGER Expenses_bir 
BEFORE INSERT ON Expenses 
FOR EACH ROW
BEGIN
  SELECT Expenses_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Reference: Expenses_Person (table: Expenses)
ALTER TABLE Expenses ADD CONSTRAINT Expenses_Person
    FOREIGN KEY (Person_ID)
    REFERENCES Person (ID)
    on delete cascade;

-- Table: Bill
CREATE TABLE Bill (
    ID NUMBER(10) NOT NULL,
    CDATE date  NOT NULL,
    EXPENSES_ID NUMBER(10)  NOT NULL,
    CONSTRAINT Bill_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE Bill_seq START WITH 1;

CREATE OR REPLACE TRIGGER Bill_bir 
BEFORE INSERT ON Bill 
FOR EACH ROW
BEGIN
  SELECT Bill_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Reference: Bill_Expenses (table: Bill)
ALTER TABLE Bill ADD CONSTRAINT Bill_Expenses
    FOREIGN KEY (Expenses_ID)
    REFERENCES Expenses (ID)
    on delete cascade;

-- Table: consists
CREATE TABLE consists (
    ID NUMBER(10)  NOT NULL,
    PRODUCT1_ID NUMBER(10)  NOT NULL,
    PRODUCT2_ID NUMBER(10)  NOT NULL,
    CONSTRAINT consists_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE consists_seq START WITH 1;

CREATE OR REPLACE TRIGGER consists_bir 
BEFORE INSERT ON consists 
FOR EACH ROW
BEGIN
  SELECT consists_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/
    
-- Reference: consists_Product1 (table: consists)
ALTER TABLE consists ADD CONSTRAINT consists_Product1
    FOREIGN KEY (Product1_ID)
    REFERENCES Product (ID)
    on delete cascade;

-- Reference: consists_Product2 (table: consists)
ALTER TABLE consists ADD CONSTRAINT consists_Product2
    FOREIGN KEY (Product2_ID)
    REFERENCES Product (ID)
    on delete cascade;

-- Table: Entry
CREATE TABLE Entry (
    BILL_ID number(10)  NOT NULL,
    PRODUCT_ID number(10)  NOT NULL,
    QUANTITY number(10)  NOT NULL,
    CONSTRAINT Entry_pk PRIMARY KEY (Bill_ID)
) ;

-- Reference: Entry_Bill (table: Entry)
ALTER TABLE Entry ADD CONSTRAINT Entry_Bill
    FOREIGN KEY (Bill_ID)
    REFERENCES Bill (ID)
    on delete cascade;

-- Reference: Entry_Product (table: Entry)
ALTER TABLE Entry ADD CONSTRAINT Entry_Product
    FOREIGN KEY (Product_ID)
    REFERENCES Product (ID)
    on delete cascade;

-- Table: sells
CREATE TABLE sells (
    ID NUMBER(10)  NOT NULL,
    PRODUCT_ID NUMBER(10)  NOT NULL,
    SELLER_ID NUMBER(10)  NOT NULL,
    CONSTRAINT sells_pk PRIMARY KEY (ID)
) ;

CREATE SEQUENCE sells_seq START WITH 1;

CREATE OR REPLACE TRIGGER sells_bir 
BEFORE INSERT ON sells 
FOR EACH ROW
BEGIN
  SELECT sells_seq.NEXTVAL
  INTO   :new.id
  FROM   dual;
END;
/

-- Reference: sells_Product (table: sells)
ALTER TABLE sells ADD CONSTRAINT sells_Product
    FOREIGN KEY (Product_ID)
    REFERENCES Product (ID)
    on delete cascade;

-- Reference: sells_Seller (table: sells)
ALTER TABLE sells ADD CONSTRAINT sells_Seller
    FOREIGN KEY (Seller_ID)
    REFERENCES Seller (ID)
    on delete cascade;

-- View: Products
CREATE VIEW Products AS
select
SELLER.NAME as SELLER,
SELLER.RATING as SELLERRATING,
PRODUCT.NAME,
PRODUCT.PRICE,
PRODUCT.DESCRIPTION,
SELLER.ID as SELLER_ID,
PRODUCT.ID as PRODUCT_ID
from SELLS
left join SELLER
on SELLS.SELLER_ID = SELLER.ID
left join PRODUCT
on SELLS.PRODUCT_ID = PRODUCT.ID;

CREATE VIEW SAFE
select PERSON.ID, PERSON.NAME, SUM( ACCOUNT.AMOUNT ) as Account, SUM( EXPENSES.AMOUNT ) as Expenses
from PERSON
left join ACCOUNT on PERSON.ID = ACCOUNT.Person_ID
left join EXPENSES on PERSON.ID = EXPENSES.Person_ID
group by NAME

-- End of file.
