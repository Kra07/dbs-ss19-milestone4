import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Oracledb{

	public void insertQ( String valvar, String val, String table ) {
		String database = "jdbc:oracle:thin:@oracle-lab.cs.univie.ac.at:1521:lab";
		String user = "a01227531";
		String pass = "dbs19";
		Connection conn = null;
		Statement stmt = null;
		
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			conn = DriverManager.getConnection(database, user, pass);
			stmt = conn.createStatement();

			try {
				int rowsAffected = stmt.executeUpdate( "INSERT INTO "+table+" ("+valvar+") VALUES ("+val+")" );
			} catch (Exception e) {
				System.err.println("Error while executing INSERT INTO statement: " + e.getMessage());
			}

			ResultSet rs = stmt.executeQuery("SELECT COUNT(*) FROM " + table );
			if (rs.next()) {
				int count = rs.getInt(1);
				System.out.println("Number of datasets: " + count);
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

}
