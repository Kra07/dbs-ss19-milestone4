import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class main {
	
	public static void main(String[] args)	{
		Oracledb con = new Oracledb();

		fill_person( con );
		fill_seller( con );
		fill_product( con );

		fill_account( con );
		fill_expenses( con );

		fill_consists( con );

		fill_bill( con );
		fill_entry( con );

		fill_sells( con );
	}

	public static void fill_account( Oracledb con ) {
		List<String[]> input = read( "tables/ACCOUNT.csv" );

		for (String[] line : input ) {
			con.insertQ( "AMOUNT, CDATE, PERSON_ID", "'"+line[0]+"', '"+line[1]+"', '"+line[2]+"'", "ACCOUNT" );
		}
	}

	public static void fill_bill( Oracledb con ) {
		List<String[]> input = read( "tables/BILL.csv" );

		for (String[] line : input ) {
			con.insertQ( "CDATE, EXPENSES_ID", "'"+line[0]+"', '"+line[1]+"'", "BILL" );
		}
	}

	public static void fill_consists( Oracledb con ) {
		List<String[]> input = read( "tables/CONSISTS.csv" );

		for (String[] line : input ) {
			con.insertQ( "PRODUCT1_ID, PRODUCT2_ID", "'"+line[0]+"', '"+line[1]+"'", "CONSISTS" );
		}
	}

	public static void fill_entry( Oracledb con ) {
		List<String[]> input = read( "tables/ENTRY.csv" );

		for (String[] line : input ) {
			con.insertQ( "BILL_ID, PRODUCT_ID, QUANTITY", "'"+line[0]+"', '"+line[1]+"', '"+line[2]+"'", "ENTRY" );
		}
	}

	public static void fill_expenses( Oracledb con ) {
		List<String[]> input = read( "tables/EXPENSES.csv" );

		for (String[] line : input ) {
			con.insertQ( "AMOUNT, CDATE, PERSON_ID", "'"+line[0]+"', '"+line[1]+"', '"+line[2]+"'", "EXPENSES" );
		}
	}

	public static void fill_person( Oracledb con ) {
		List<String[]> input = read( "tables/PERSON.csv" );

		for (String[] line : input ) {
			con.insertQ( "NAME, SHORTNAME", "'"+line[0]+"', '"+line[1]+"'", "PERSON" );
		}
	}

	public static void fill_product( Oracledb con ) {
		List<String[]> input = read( "tables/PRODUCT.csv" );

		for (String[] line : input ) {
			con.insertQ( "NAME, DESCRIPTION, PRICE", "'"+line[0]+"', '"+line[1]+"', '"+line[2]+"'", "PRODUCT" );
		}
	}

	public static void fill_seller( Oracledb con ) {
		List<String[]> input = read( "tables/SELLER.csv" );

		for (String[] line : input ) {
			con.insertQ( "NAME, RATING", "'"+line[0]+"', '"+line[1]+"'", "SELLER" );
		}
	}

	public static void fill_sells( Oracledb con ) {
		List<String[]> input = read( "tables/SELLS.csv" );

		for (String[] line : input ) {
			con.insertQ( "PRODUCT_ID, SELLER_ID", "'"+line[0]+"', '"+line[1]+"'", "SELLS" );
		}
	}

	public static List<String[]> read( String file ){
		List<String[]> records = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] values = line.split(",");
				records.add(values);
			}
		}
		catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return records;
	}

}
